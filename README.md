# Crypto

## Running application

```
gradlew bootRun
```

Go to [Swagger](https://damp-cliffs-96306.herokuapp.com/swagger-ui.html) resources

### Prerequisites

You will need Java 11

## Running the tests

```
gradlew test
```

## Built With

* Java11
* Spring Boot
* Lombok
* SpringFox
* JUnit