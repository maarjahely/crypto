package ee.mindly.crypto.repository;

import ee.mindly.crypto.entity.CryptoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CryptoRepository extends JpaRepository<CryptoEntity, Long> {
}
