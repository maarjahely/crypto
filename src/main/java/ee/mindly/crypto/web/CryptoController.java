package ee.mindly.crypto.web;

import ee.mindly.crypto.dto.Crypto;
import ee.mindly.crypto.dto.CryptoRequest;
import ee.mindly.crypto.service.CryptoService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "https://thawing-eyrie-38168.herokuapp.com")
@AllArgsConstructor
@RestController
@RequestMapping("/")
public class CryptoController {
    private final CryptoService cryptoService;

    @ApiOperation(value = "Add crypto to database")
    @PostMapping(value = "crypto", consumes = {"application/json"})
    public Long addCrypto(@RequestBody @Valid CryptoRequest cryptoRequest) {
        return cryptoService.addCrypto(cryptoRequest);
    }

    @ApiOperation(value = "Get all cryptos")
    @GetMapping(value = "crypto")
    public List<Crypto> getCryptos() {
        return cryptoService.getCryptos();
    }

    @ApiOperation(value = "Delete crypto by id")
    @DeleteMapping("crypto/{cryptoId}")
    public void deleteCrypto(@PathVariable Long cryptoId) {
        cryptoService.deleteCrypto(cryptoId);
    }

}
