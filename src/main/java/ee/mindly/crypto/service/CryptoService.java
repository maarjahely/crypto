package ee.mindly.crypto.service;

import ee.mindly.crypto.dto.Crypto;
import ee.mindly.crypto.dto.CryptoRequest;
import ee.mindly.crypto.entity.CryptoEntity;
import ee.mindly.crypto.repository.CryptoRepository;
import ee.mindly.crypto.types.CryptoType;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
@AllArgsConstructor
@Service
public class CryptoService {
    private final CryptoRepository cryptoRepository;
    private final BitfinexService bitfinexService;

    public Long addCrypto(CryptoRequest cryptoRequest) {
        log.info("Saving crypto {}", cryptoRequest);

        CryptoEntity cryptoEntity = CryptoEntity.builder()
                .amount(cryptoRequest.getAmount())
                .cryptoType(cryptoRequest.getCryptoType().name())
                .walletLocation(cryptoRequest.getWalletLocation())
                .purchaseDate(cryptoRequest.getPurchaseDate())
                .build();

        cryptoRepository.save(cryptoEntity);
        return cryptoEntity.getId();
    }

    public List<Crypto> getCryptos() {
        log.info("Getting all cryptos");

        List<CryptoEntity> cryptoEntities = cryptoRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        Set<CryptoType> usedCryptoTypes = cryptoEntities.stream()
                .map(cryptoEntity -> CryptoType.valueOf(cryptoEntity.getCryptoType()))
                .collect(Collectors.toSet());
        Map<CryptoType, BigDecimal> cryptoValues = new HashMap<>();

        usedCryptoTypes.forEach(cryptoType -> cryptoValues
                .put(cryptoType, bitfinexService.getCryptoMarketValue(cryptoType)));

        return cryptoEntities.stream()
                .map(cryptoEntity -> mapDataToApi(cryptoEntity, cryptoValues))
                .collect(Collectors.toList());
    }

    private Crypto mapDataToApi(CryptoEntity cryptoEntity, Map<CryptoType, BigDecimal> cryptoValues) {
        BigDecimal marketValue = cryptoEntity.getAmount()
                .multiply(cryptoValues.get(CryptoType.valueOf(cryptoEntity.getCryptoType())));

        return Crypto.builder()
                .id(cryptoEntity.getId())
                .amount(cryptoEntity.getAmount())
                .cryptoType(CryptoType.valueOf(cryptoEntity.getCryptoType()))
                .walletLocation(cryptoEntity.getWalletLocation())
                .purchaseDate(cryptoEntity.getPurchaseDate())
                .marketValue(marketValue)
                .build();
    }

    public void deleteCrypto(Long cryptoId) {
        log.info("Deleting crypto {}", cryptoId);
        cryptoRepository.delete(getCryptoById(cryptoId));
    }

    private CryptoEntity getCryptoById(Long cryptoId) {
        return cryptoRepository.findById(cryptoId)
                .orElseThrow(() -> new RuntimeException("No crypto found with cryptoId " + cryptoId));
    }
}
