package ee.mindly.crypto.service;

import ee.mindly.crypto.dto.BitFinexRequest;
import ee.mindly.crypto.types.CryptoType;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.util.Objects;

@Log4j2
@AllArgsConstructor
@Service
public class BitfinexService {
    private final RestTemplate restTemplate;

    @Cacheable("marketValue")
    public BigDecimal getCryptoMarketValue(CryptoType cryptoType) {
        log.info("Getting crypto market value for {}", cryptoType);
        BitFinexRequest request = BitFinexRequest.builder()
                .ccy1(cryptoType.getValue())
                .ccy2("EUR")
                .build();

        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme("https").host("api.bitfinex.com").path("/v2/calc/fx")
                .build();
        ResponseEntity<BigDecimal[]> result = restTemplate.postForEntity(uriComponents.toUriString(), request, BigDecimal[].class);
        return Objects.requireNonNull(result.getBody())[0];
    }


    @CacheEvict(allEntries = true, cacheNames = {"marketValue"})
    @Scheduled(fixedDelay = 300000)
    public void cacheEvict() {
        log.info("Evicting cache");
    }
}
