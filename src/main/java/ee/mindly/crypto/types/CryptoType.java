package ee.mindly.crypto.types;

public enum CryptoType {
    BITCOIN("BTC"),
    ETHEREUM("ETH"),
    RIPPLE("XRP");
    private String value;

    CryptoType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
