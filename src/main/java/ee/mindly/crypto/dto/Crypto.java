package ee.mindly.crypto.dto;

import ee.mindly.crypto.types.CryptoType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Crypto {
    private Long id;
    private CryptoType cryptoType;
    private BigDecimal amount;
    private LocalDate purchaseDate;
    private String walletLocation;
    private BigDecimal marketValue;
}
