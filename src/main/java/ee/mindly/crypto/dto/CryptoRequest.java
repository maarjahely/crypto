package ee.mindly.crypto.dto;

import ee.mindly.crypto.types.CryptoType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CryptoRequest {
    @NotNull
    private CryptoType cryptoType;
    @NotNull
    private BigDecimal amount;
    @NotNull
    private LocalDate purchaseDate;
    @NotBlank
    private String walletLocation;
}
