package ee.mindly.crypto.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "crypto")
public class CryptoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "crypto_type")
    private String cryptoType;
    @Column(name = "amount")
    private BigDecimal amount;
    @Column(name = "purchase_date")
    private LocalDate purchaseDate;
    @Column(name = "wallet_location")
    private String walletLocation;
}
