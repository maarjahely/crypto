package ee.mindly.crypto.service;

import ee.mindly.crypto.dto.Crypto;
import ee.mindly.crypto.dto.CryptoRequest;
import ee.mindly.crypto.entity.CryptoEntity;
import ee.mindly.crypto.repository.CryptoRepository;
import ee.mindly.crypto.types.CryptoType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Sort;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class CryptoServiceTest {

    @Mock
    private CryptoRepository cryptoRepository;

    @Mock
    private BitfinexService bitfinexService;

    @Captor
    private ArgumentCaptor<CryptoEntity> cryptoEntityArgumentCaptor;

    @Mock
    private CryptoEntity cryptoEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void addCrypto() {
        CryptoRequest cryptoRequest = CryptoRequest.builder()
                .cryptoType(CryptoType.BITCOIN)
                .amount(BigDecimal.TEN)
                .purchaseDate(LocalDate.of(2020, 1, 20))
                .walletLocation("Wallet")
                .build();

        createService().addCrypto(cryptoRequest);
        verify(cryptoRepository).save(cryptoEntityArgumentCaptor.capture());
        CryptoEntity cryptoEntity = cryptoEntityArgumentCaptor.getValue();

        assertEquals(cryptoRequest.getAmount(), cryptoEntity.getAmount());
        assertEquals(cryptoRequest.getCryptoType().name(), cryptoEntity.getCryptoType());
        assertEquals(cryptoRequest.getPurchaseDate(), cryptoEntity.getPurchaseDate());
        assertEquals(cryptoRequest.getWalletLocation(), cryptoEntity.getWalletLocation());
    }

    @Test
    void getCryptos() {
        CryptoEntity cryptoEntity = CryptoEntity.builder()
                .cryptoType(CryptoType.BITCOIN.name())
                .amount(BigDecimal.TEN)
                .id(1L)
                .purchaseDate(LocalDate.of(2020, 01, 20))
                .walletLocation("Wallet")
                .build();
        when(cryptoRepository.findAll(Sort.by(Sort.Direction.ASC, "id")))
                .thenReturn(Collections.singletonList(cryptoEntity));
        when(bitfinexService.getCryptoMarketValue(CryptoType.BITCOIN))
                .thenReturn(BigDecimal.valueOf(1.5));

        List<Crypto> result = createService().getCryptos();
        Crypto crypto = result.get(0);
        assertEquals(cryptoEntity.getAmount(), crypto.getAmount());
        assertEquals(cryptoEntity.getCryptoType(), crypto.getCryptoType().name());
        assertEquals(cryptoEntity.getPurchaseDate(), crypto.getPurchaseDate());
        assertEquals(cryptoEntity.getWalletLocation(), crypto.getWalletLocation());
        assertEquals(BigDecimal.valueOf(15.0), crypto.getMarketValue());
    }

    @Test
    void deleteCryptoExists() {
        when(cryptoRepository.findById(1L)).thenReturn(Optional.of(cryptoEntity));
        createService().deleteCrypto(1L);
        verify(cryptoRepository).delete(cryptoEntity);
    }

    @Test
    void deleteCryptoNotExists() {
        when(cryptoRepository.findById(1L)).thenReturn(Optional.empty());

        RuntimeException assertThrows = assertThrows(RuntimeException.class,
                () -> createService().deleteCrypto(1L));
        assertEquals("No crypto found with cryptoId 1", assertThrows.getMessage());
    }

    private CryptoService createService() {
        return new CryptoService(cryptoRepository, bitfinexService);
    }
}